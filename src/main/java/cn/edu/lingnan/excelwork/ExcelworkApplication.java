package cn.edu.lingnan.excelwork;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("cn.edu.lingnan.excelwork.dao")
public class ExcelworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExcelworkApplication.class, args);
    }

}
