package cn.edu.lingnan.excelwork.controller;

import cn.edu.lingnan.excelwork.entity.Student;
import cn.edu.lingnan.excelwork.service.StudentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * (Student)表控制层
 *
 * @author makejava
 * @since 2020-05-25 23:50:09
 */
@Controller
@RequestMapping("student")
public class StudentController {
    /**
     * 服务对象
     */
    @Resource
    private StudentService studentService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Student selectOne(Integer id) {
        return this.studentService.queryById(id);
    }

    @RequestMapping("index")
    public String index(){
        return "index";
    }

    /**
     * 导入Excel
     */


    @RequestMapping("import")
    @ResponseBody
    public String excelImport(@RequestParam(value="filename") MultipartFile file, HttpSession session) {

//		String fileName = file.getOriginalFilename();

        int result = 0;

        try {
            result = studentService.addUser(file);
        } catch (Exception e) {

            e.printStackTrace();
        }

        if (result != 0) {
            return "excel文件数据导入成功！";
        } else {
            return "excel数据导入失败！";
        }
    }

}