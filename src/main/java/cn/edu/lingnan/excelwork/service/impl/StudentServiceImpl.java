package cn.edu.lingnan.excelwork.service.impl;

import cn.edu.lingnan.excelwork.entity.Student;
import cn.edu.lingnan.excelwork.dao.StudentDao;
import cn.edu.lingnan.excelwork.service.StudentService;
import cn.edu.lingnan.excelwork.transcation.MyException;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * (Student)表服务实现类
 *
 * @author makejava
 * @since 2020-05-25 23:50:08
 */
@Service("studentService")
public class StudentServiceImpl implements StudentService {
    @Resource
    private StudentDao studentDao;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Student queryById(Integer id) {
        return this.studentDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Student> queryAllByLimit(int offset, int limit) {
        return this.studentDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param student 实例对象
     * @return 实例对象
     */
    @Override
    public Student insert(Student student) {
        this.studentDao.insert(student);
        return student;
    }

    /**
     * 修改数据
     *
     * @param student 实例对象
     * @return 实例对象
     */
    @Override
    public Student update(Student student) {
        this.studentDao.update(student);
        return this.queryById(student.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.studentDao.deleteById(id) > 0;
    }

    @Override
    public int addUser(MultipartFile file) throws Exception {
        int result=0;
        SqlSession batchSqlSession = null;

        //存放Excel表中所有的student信息
        List<Student> studentList =new ArrayList<>();

        /**
         * 判断文件的版本
         *
         */
        String fileName= file.getOriginalFilename();
        String suffix= fileName.substring(fileName.lastIndexOf(".")+1);

        InputStream ins=file.getInputStream();
        Workbook wb=null;
        if (suffix.equals("xlsx")){
            wb=new XSSFWorkbook(ins);
        }else {
            wb=new HSSFWorkbook(ins);
        }

        /**
         * 获取Excel表单
         */

        Sheet sheet=wb.getSheetAt(0);

        /**
         * line=2:从表的第三行开始获取记录
         */

        if (null!=sheet){
            for (int line=2;line<=sheet.getLastRowNum();line++){
                Student student=new Student();

                Row row=sheet.getRow(line);
                if (null==row){
                    continue;
                }

                /**
                 * 判断单元格类型是否为文本类型
                 */

                if(row.getCell(0).getCellType()!= 1){
                    /**
                     * 获取第一个单元格的内容
                     */
                    row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
                    String id = row.getCell(0).getStringCellValue();

                    int sid=Integer.parseInt(id);

                    row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
                    /**
                     * 获取第二个单元格的内容
                     */

                    String sno = row.getCell(1).getStringCellValue();

                    row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);

                    String sname=row.getCell(2).getStringCellValue();

                    row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);

                    String sclass= row.getCell(3).getStringCellValue();
                    row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
                    String remarks=row.getCell(4).getStringCellValue();
                    student.setId(sid);
                    student.setSno(sno);
                    student.setSname(sname);
                    student.setSclass(sclass);
                    student.setRemarks(remarks);
                    studentList.add(student);

                }


            }

           // 每次批量插入的数量
            int batchCount = 5;
//
            int batchLastIndex = batchCount;//每批最后一条数据的下标

            batchSqlSession = this.sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH,false);
            StudentDao studentDao=batchSqlSession.getMapper(StudentDao.class);

            /**
             * 批量插入
             */

            for(int index = 0; index < studentList.size();){
                /**
                 * 如果数据量小于设置的批量数量，则最后的下标值为实际数量
                 */
                if(batchLastIndex >= studentList.size()){

                    batchLastIndex = studentList.size();
                    result = result + studentDao.importBatchInsert(studentList.subList(index,batchLastIndex));
//					清楚缓存
                    batchSqlSession.clearCache();
                    break;

                }else {

                    result = result + studentDao.importBatchInsert(studentList.subList(index, batchLastIndex));

                    batchSqlSession.clearCache();

                    index = batchLastIndex;

                    batchLastIndex = index + (batchCount -1);

                }

            }
//			将数据提交到数据库，否则执行但是未将数据插入到数据库
            batchSqlSession.commit();
        }

        return result;
    }
}